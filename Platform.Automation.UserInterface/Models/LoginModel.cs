﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;

namespace Platform.Automation.UserInterface.Models
{
    public class LoginModel : BindableBase, IViewModelControl
    {

        private string _validationMessage = "";

        public string PlatformUsername { get; set; }
        public string PlatformUrl { get; set; }

        public ICommand LoginCommand { get; set; }

        public AutomationService AutomationService { get; set; }

        public LoginModel()
        {
            LoginCommand = new RelayCommand(x => LoginExecute(x));
        }

        public LoginModel(object dataContext)
        {
            AutomationService = ((AutomationService)dataContext);
            LoginCommand = new RelayCommand(x => LoginExecute(x));
        }

        public async void LoginExecute(object passwordbox)
        {
            try
            {
                var result = await AutomationService.CredentialRotationManager.Initialize(PlatformUrl, PlatformUsername, ((WatermarkPasswordBox)passwordbox).Password);
                AutomationService.ErrorLog.Add(result);

                if (result.Contains("Completed") && result.Contains("Created"))
                {
                    AutomationService.NavigateNext();
                }
                else
                {
                    ValidationMessage = result;
                }
            }
            catch (Exception ex)
            {
                ValidationMessage = ex.Message;
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                if (_validationMessage != value)
                {
                    SetProperty(ref _validationMessage, value);
                    RaisePropertyChanged();
                }
            }
        }

    }
}
