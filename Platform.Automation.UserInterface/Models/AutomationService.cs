﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;
using Platform.Automation.Service;
using Prism;
using Prism.Common;
using Prism.Mvvm;

namespace Platform.Automation.UserInterface.Models
{
    public class AutomationService : BindableBase
    {

        public Uri CurrentSource { get; set; }
        public List<string> ErrorLog { get; set; }

        private IViewModelControl currentWorkflow;
        private List<IViewModelControl> automationWorkflows;

        public CredentialRotation CredentialRotationManager { get; set; }

        public AutomationService()
        {
            CredentialRotationManager = new CredentialRotation();
            ErrorLog = new List<string>();
            automationWorkflows = new List<IViewModelControl>();

            // Instansiate the workflows:
            AutomationWorkflows.Add(new LoginModel(this));
            AutomationWorkflows.Add(new CredentialModel(this));
            AutomationWorkflows.Add(new PlatformTasksModel(this));

            CurrentWorkflow = AutomationWorkflows[0];
        }

        public void NavigateNext()
        {
            var index = AutomationWorkflows.IndexOf(CurrentWorkflow);
            CurrentWorkflow = AutomationWorkflows[++index];
        }

        public void NavigateLast()
        {
            var index = AutomationWorkflows.IndexOf(CurrentWorkflow);
            CurrentWorkflow = AutomationWorkflows[--index];
        }

        public IViewModelControl CurrentWorkflow
        {
            get { return currentWorkflow; }
            set
            {
                if (currentWorkflow != value)
                {
                    SetProperty(ref currentWorkflow, value);
                    RaisePropertyChanged();
                }
            }
        }

        public List<IViewModelControl> AutomationWorkflows
        {
            get { return automationWorkflows; }
        }
    }
}
