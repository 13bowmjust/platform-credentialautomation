﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Platform.Automation.UserInterface.Models
{
    public class CredentialModel : BindableBase, IViewModelControl
    {
        //TODO: setup model to get passwords after typing is complete... each CredentialChange model has a masked
        // Password.


        private string _validationMessage = "";
        private ObservableCollection<CredentialChange> _credentialChanges;

        public AutomationService AutomationService { get; set; }

        public ICommand NextCommand { get; set; }
        public ICommand BackCommand { get; set; }
        public ICommand AddCommand { get; set; }

        public CredentialModel()
        {

        }

        public CredentialModel(AutomationService service)
        {
            AutomationService = service;
            _credentialChanges = new ObservableCollection<CredentialChange>();
            NextCommand = new RelayCommand(x => Next());
            BackCommand = new RelayCommand(x => Back());
            AddCommand = new RelayCommand(x => AddCredential());
        }

        public void Next()
        {
            // Logic for adding the Credentials into the AutomationService:


            AutomationService.NavigateNext();
        }

        public void Back()
        {
            AutomationService.NavigateLast();
        }

        public void AddCredential()
        {
            CredentialChanges.Add(new CredentialChange());
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<CredentialChange> CredentialChanges
        {
            get { return _credentialChanges; }
            set
            {
                SetProperty(ref _credentialChanges, value);
                RaisePropertyChanged();
            }
        }
    }
}
