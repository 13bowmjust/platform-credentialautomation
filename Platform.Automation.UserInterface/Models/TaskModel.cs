﻿using Platform.Automation.Service.Tasks;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Automation.UserInterface.Models
{
    public class TaskModel : BindableBase
    {
        // TODO: Setup to add taskComponents from child objects, better constructor suppot.

        public TaskOverviewModel TaskMainOverview { get; set; }
        public ObservableCollection<TaskOverviewModel> _taskComponents;
        public ServiceTask Task { get; set; }

        public TaskModel()
        {
            TaskMainOverview = new TaskOverviewModel();
            TaskComponents = new ObservableCollection<TaskOverviewModel>();
        }

        public TaskModel(TaskOverviewModel taskMain, 
            List<TaskOverviewModel> taskComponents, 
            ServiceTask task)
        {
            TaskMainOverview = taskMain;
            TaskComponents = new ObservableCollection<TaskOverviewModel>();
            TaskComponents.AddRange(taskComponents);
            Task = task;
        }

        public ObservableCollection<TaskOverviewModel> TaskComponents
        {
            get { return _taskComponents; }
            set
            {
                SetProperty(ref _taskComponents, value);
                RaisePropertyChanged();
            }
        }
    }
}
