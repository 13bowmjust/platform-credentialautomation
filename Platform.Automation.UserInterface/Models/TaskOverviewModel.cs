﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media;
using Prism.Mvvm;
using Platform.Automation.Service.Data;

namespace Platform.Automation.UserInterface.Models
{
    public class TaskOverviewModel : BindableBase, IViewModelControl
    {

        public int TaskIndex { get; set; }
        public string TaskAlias { get; set; }

        private Icon _taskIcon;
        private string _validationMessage = "";

        public TaskOverviewModel()
        {
            TaskIcon = SystemIcons.Application;
            TaskAlias = "Default Application Alias";
        }

        public TaskOverviewModel(int index, string alias)
        {
            TaskIndex = index;
            TaskAlias = alias;
            TaskIcon = SystemIcons.Question;
        }

        public ImageSource TaskIconSource
        {
            get { return TaskIcon.ToImageSource(); }
        }

        public Icon TaskIcon
        {
            get { return _taskIcon; }
            set
            {
                SetProperty(ref _taskIcon, value);
                RaisePropertyChanged("TaskIconSource");
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }
    }
}
