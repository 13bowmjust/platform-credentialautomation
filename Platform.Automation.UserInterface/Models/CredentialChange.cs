﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Automation.UserInterface.Models
{
    public class CredentialChange : BindableBase
    {

        private string _previousCredential;
        private string _newDomain;
        private string _newUsername;
        private string _newPassword;

        public CredentialChange()
        {
            _previousCredential = "";
            _newDomain = "";
            _newUsername = "";
            _newPassword = "";
        }

        public string PreviousCredential
        {
            get { return _previousCredential; }
            set
            {
                if (_previousCredential != value)
                {
                    SetProperty(ref _previousCredential, value);
                    RaisePropertyChanged();
                }
            }
        }

        public string NewDomain
        {
            get { return _newDomain; }
            set
            {
                if (_newDomain != value)
                {
                    SetProperty(ref _newDomain, value);
                    RaisePropertyChanged();
                }
            }
        }

        public string NewUsername
        {
            get { return _newUsername; }
            set
            {
                if (_newUsername != value)
                {
                    SetProperty(ref _newUsername, value);
                    RaisePropertyChanged();
                }
            }
        }

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                if (_newPassword != value)
                {
                    SetProperty(ref _newPassword, value);
                    RaisePropertyChanged();
                }
            }
        }

    }
}
