﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Automation.UserInterface.Models
{
    public interface IViewModelControl
    {

        string ValidationMessage { get; set; }

    }
}
