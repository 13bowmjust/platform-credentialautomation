﻿using Platform.Automation.UserInterface.ViewObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Mvvm;
using System.Drawing;

namespace Platform.Automation.UserInterface.Models
{
    public class PlatformTasksModel : BindableBase, IViewModelControl
    {

        private ObservableCollection<TaskModel> _platformTasks;
        private string _btnText;
        private ICommand _omniCommand;

        public string ValidationMessage { get; set; }

        public ICommand BackCommand { get; set; }

        public AutomationService AutomationService { get; set; }

        public PlatformTasksModel()
        {

        }

        public PlatformTasksModel(AutomationService service)
        {
            AutomationService = service;
            PlatformTasks = new ObservableCollection<TaskModel>();
            OmniCommand = new RelayCommand(x => Identify());
            BackCommand = new RelayCommand(x => Back());

            // Initialize button text:
            TaskButtonText = "Identify";
        }

        public async void Identify()
        {
            // Idempotency - Clear list before starting in case it ran twice...:
            PlatformTasks.Clear();

            AutomationService.CredentialRotationManager.MappedAdminCredentials.Add("acpadmin", new Apprenda.Utility.Windows.Credentials("appjb.local", "acpadmin", "304532Jrb"));
            var result = await AutomationService.CredentialRotationManager.IdentifyTasksAsync();
            var indexTask = 1;

            // Task Results:
            foreach (var task in result.Tasks)
            {
                // Task Information:
                foreach (var value in task.Value)
                {
                    var mainTask = new TaskOverviewModel(indexTask, value.ApplicationName);
                    var componentTasks = new List<TaskOverviewModel>();

                    // TODO: Implement this section to get components;
                    var indexComponent = 1;

                    // Task Components:
                    foreach (var component in value.ImplementationGroupResults)
                    {
                        componentTasks.Add(new TaskOverviewModel(indexComponent, component.Name));
                        indexComponent++;
                    }
                    foreach (var presentation in value.VersionPresentationResults)
                    {
                        componentTasks.Add(new TaskOverviewModel(indexComponent, presentation.Name));
                        indexComponent++;
                    }

                    PlatformTasks.Add(new TaskModel(mainTask, componentTasks, value));
                    indexTask++;
                }
            }

            // Switch tasks to Execute Method:
            TaskButtonText = "Execute";
            OmniCommand = new RelayCommand(x => Execute());
        }

        public async void Execute()
        {
            // Set Platform Credentials if need be:
            //if (AutomationService.CredentialRotationManager.MappedAdminCredentials.Count != 0)
            //{
            //    var adminCredentialsResult = await AutomationService.CredentialRotationManager.ServiceManager.SetPlatformAdminCredentialsAsync(AutomationService.CredentialRotationManager.MappedAdminCredentials.Values.ToList());
            //}

            // Execute each task from the PlatformTasks:
            foreach (var model in PlatformTasks)
            {
                // Normal routine, cycle the workload:
                var artifacts = model.Task.DeployedVersionArtifactResults.D.Results.FindAll(x => x.ComponentType == 1 || x.ComponentType == 3);
                var taskResult = true;

                //If the Task does NOT use Default Credentials - Update the custom creds first:
                if (!model.Task.UsesDefaultCredentials)
                {

                }

                // Components:
                for (var index = 0; index < artifacts.Count; index++)
                {
                    // Attempt to cycle the workload component:
                    var response = await AutomationService.CredentialRotationManager.CycleWorkloadAsync(artifacts[index]);

                    // Log the result and figure out the course of action:
                    AutomationService.ErrorLog.Add(response);

                    var result = response.Contains("Deploy status: Success") && response.Contains("Undeploy status: Success");

                    if (result)
                    {
                        // If success - change icon to Success:
                        model.TaskComponents[index].TaskIcon = SystemIcons.Shield;
                    }
                    else
                    {
                        // If failure - change icon to Error:
                        // DON'T BREAK, no point in breaking such an operation to have the
                        // USER try again... this could be the last component and have ran
                        // for serveral hours! Manual intervention may be needed:
                        model.TaskComponents[index].TaskIcon = SystemIcons.Error;
                        taskResult = false;
                    }
                }

                // Mark the primary task as Success or Failure:
                if (taskResult)
                {
                    model.TaskMainOverview.TaskIcon = SystemIcons.Shield;
                }
                else
                {
                    model.TaskMainOverview.TaskIcon = SystemIcons.Error;
                }
            }
        }

        public void Back()
        {
            AutomationService.NavigateLast();
            //PlatformTasks[0].TaskMainOverview.TaskIcon = SystemIcons.Warning;
        }

        public ObservableCollection<TaskModel> PlatformTasks
        {
            get { return _platformTasks; }
            set
            {
                SetProperty(ref _platformTasks, value);
                RaisePropertyChanged();
            }
        }

        public ICommand OmniCommand
        {
            get { return _omniCommand; }
            set
            {
                SetProperty(ref _omniCommand, value);
                RaisePropertyChanged();
            }
        }

        public string TaskButtonText
        {
            get { return _btnText; }
            set
            {
                SetProperty(ref _btnText, value);
                RaisePropertyChanged();
            }
        }
    }
}
