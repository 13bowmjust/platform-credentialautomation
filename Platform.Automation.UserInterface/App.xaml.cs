﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Platform.Automation.UserInterface.Models;

namespace Platform.Automation.UserInterface
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow app = new MainWindow();
            AutomationService serviceContext = new AutomationService();
            app.DataContext = serviceContext;
            app.Show();
        }

    }
}
