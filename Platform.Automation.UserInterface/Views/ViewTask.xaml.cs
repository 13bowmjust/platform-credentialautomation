﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Platform.Automation.Service;
using Platform.Automation.UserInterface.Models;
using Platform.Automation.Service.Data;
using Platform.Automation.UserInterface.ViewObjects;
using Apprenda.Utility.Windows;

namespace Platform.Automation.UserInterface.Views
{
    /// <summary>
    /// Interaction logic for ViewTask.xaml
    /// </summary>
    public partial class ViewTask : UserControl
    {

        public ViewTask()
        {
            InitializeComponent();
        }
    }
}
