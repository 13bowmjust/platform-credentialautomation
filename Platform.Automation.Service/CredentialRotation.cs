﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apprenda.Utility.Windows;
using Platform.Automation.Service.Data;
using Platform.Automation.Service.Tasks;
using RestSharp;

namespace Platform.Automation.Service
{
    public class CredentialRotation
    {

        public ServiceAccountManager ServiceManager { get; set; }
        public PlatformTask CredentialTask { get; set; }

        public Dictionary<string, Credentials> MappedCredentialChanges { get; set; }
        public Dictionary<string, Credentials> MappedAdminCredentials { get; set; }

        public CredentialRotation()
        {
            ServiceManager = new ServiceAccountManager();
            CredentialTask = new PlatformTask();

            MappedCredentialChanges = new Dictionary<string, Credentials>();
            MappedAdminCredentials = new Dictionary<string, Credentials>();
        }

        public CredentialRotation(List<Credentials> customCredentials, List<Credentials> adminCredentials)
        {
            ServiceManager = new ServiceAccountManager();
            CredentialTask = new PlatformTask();

            MappedCredentialChanges = new Dictionary<string, Credentials>();
            MappedAdminCredentials = new Dictionary<string, Credentials>();

            // TODO: Update to be mapping Credential -> Credential rather than just string:
            if (customCredentials != null)
            {
                foreach (var custom in customCredentials)
                {
                    MappedCredentialChanges.Add(custom.UserName, custom);
                }
            }

            if (adminCredentials != null)
            {
                foreach (var admin in adminCredentials)
                {
                    MappedAdminCredentials.Add(admin.UserName, admin);
                }
            }
        }

        public async Task<string> Initialize(string platUrl, string username, string password)
        {
            var result = await ServiceManager.Connect(platUrl, username, password);

            return $"Login {result.ResponseStatus} - Code: {result.StatusCode} - Message: {result.ErrorMessage}";
        }

        public async Task<List<string>> ExecuteTasks()
        {
            // Actual iterate through TASKS and apply the needed changes:
            var executionReport = new List<string>();

            if (MappedAdminCredentials.Values.Count > 0)
            {

                // Update Platform Registry Credentials:
                executionReport.Add(
                    await ServiceManager.SetPlatformAdminCredentialsAsync(MappedAdminCredentials.Values.ToList()));

                // Filter: Apps Using Registry Credentials:
                foreach (var defaultTask in CredentialTask.Tasks["Default"])
                {
                    // Cycle and add results to report:
                    executionReport.Add(await CycleWorkloadFromTaskAsync(defaultTask));
                }

                foreach (var apprendaTask in CredentialTask.Tasks["Apprenda"])
                {
                    // Cycle and add results to report:
                    executionReport.Add(await CycleWorkloadFromTaskAsync(apprendaTask));
                }
            }

            if (MappedCredentialChanges.Values.Count > 0)
            {
                // Apps using Custom Credentials:
                foreach (var customTask in CredentialTask.Tasks["Custom"])
                {
                    // Update task Service Level Options and cycle:
                    executionReport.Add(await UpdateServiceLevelOptionsFromTask(customTask));
                    executionReport.Add(await CycleWorkloadFromTaskAsync(customTask));
                }
            }

            return executionReport;
        }

        public async Task<string> UpdateServiceLevelOptionsFromTask(ServiceTask task)
        {
            // If the WCF Service or UI uses custom credentials, then process those changes and apply the updates:
            var responses = $"Updates being applied to: {task.ApplicationName}\r\n";

            // Update services first:
            foreach (var service in task.ImplementationGroupResults)
            {
                // Get the updated password from the list:
                var sla = service.CurrentImplementationGroup;
                var newCredentials = MappedCredentialChanges[service.Credentials.UserName];

                // Update the password:
                sla.Group.Password = newCredentials.Password;

                var serviceResponse = await ServiceManager.UpdateImplementationGroupAsync(sla);
                responses += $"{service.Name}: {serviceResponse}\r\n";
            }

            // Update UIs second:
            foreach (var ui in task.VersionPresentationResults)
            {
                var pla = ui.PresentationSla;
                var newCredentials = MappedCredentialChanges[ui.Credentials.UserName];

                pla.Sla.Password = newCredentials.Password;

                var uiResponse = await ServiceManager.UpdateVersionPresentationAsync(pla);
                responses += $"{ui.Name}: {uiResponse}\r\n";
            }

            return responses;
        }

        public async Task<string> CycleWorkloadFromTaskAsync(ServiceTask task)
        {
            // Response log string:
            var workloadResponse = $"Task Application: {task.ApplicationName}\r\n";

            // Get artifacts of type UI or WCF Service:
            var artifacts = task.DeployedVersionArtifactResults.D.Results.FindAll(x => x.ComponentType == 1 ||
                                                                                       x.ComponentType == 3);
            // Cycle through each workload in the found artifacts:
            foreach (var workload in artifacts)
            {
                var newWorkLoad = new DeployedVersionArtifact()
                {
                    Server = workload.HostName,
                    ArtifactId = workload.ArtifactId
                };
                var oldWorkLoad = new UndeployVersionArtifact()
                {
                    Server = workload.HostName,
                    InstanceId = workload.InstanceId
                };

                var responseDeploy = await ServiceManager.DeployVersionArtifactAsync(newWorkLoad);
                var responseUndeploy = await ServiceManager.UndeployVersionArtifactAsync(oldWorkLoad);

                workloadResponse +=
                    $"Artifact {workload.ArtifactId} Deploy status: {responseDeploy} | Instance {workload.InstanceId} Undeploy status: {responseUndeploy}\r\n";
            }

            return workloadResponse;
        }

        public async Task<string> CycleWorkloadAsync(DeployedArtifactResult workload)
        {
            var newWorkLoad = new DeployedVersionArtifact()
            {
                Server = workload.HostName,
                ArtifactId = workload.ArtifactId
            };
            var oldWorkLoad = new UndeployVersionArtifact()
            {
                Server = workload.HostName,
                InstanceId = workload.InstanceId
            };

            var responseDeploy = await ServiceManager.DeployVersionArtifactAsync(newWorkLoad);
            var responseUndeploy = await ServiceManager.UndeployVersionArtifactAsync(oldWorkLoad);

            return $"Artifact {workload.ArtifactId} Deploy status: {responseDeploy} | Instance {workload.InstanceId} Undeploy status: {responseUndeploy}\r\n";
        }

        public async Task<PlatformTask> IdentifyTasksAsync()
        {
            // Check the credentials against platform settings:

            // 1.) Get All applications from platform:
            var applications = await ServiceManager.GetApplicationsAsync();

            // IF: SERVICE -> only use apps from tenants other than Apprenda:
            var serviceApps = applications.Items.FindAll(x => x.DeveloperName != ApiHelper.ApprendaDevName);

            // IF: ADMIN -> filter apps list to just admin using adminTask filters:
            var apprendaApps = applications.Items.FindAll(x => x.DeveloperName == ApiHelper.ApprendaDevName);

            if (MappedAdminCredentials.Values.Count > 0)
            {
                // GET: Platform Credentials from Registry - COMPARE:
                var adminCredentials = await ServiceManager.GetPlatformAdminCredentialsAsync();

                // Flag for Admin Tasks:
                var containsAdminChanges =
                    ServiceAccountManager.CompareCredentials(MappedAdminCredentials.Values.ToList(), adminCredentials);

                // Identify needed tasks to be built:

                if (containsAdminChanges)
                {
                    CredentialTask.Tasks["Apprenda"].AddRange(
                        await ServiceManager.BuildServiceTasksFromApplicationsAsync(apprendaApps, apprendaApps: true));

                    CredentialTask.Tasks["Default"].AddRange(
                        await
                            ServiceManager.BuildServiceTasksFromApplicationsAsync(serviceApps,
                                MappedAdminCredentials.Values.ToList()));
                }
            }

            // IF: There are custom service credentials to search for, search here:
            if (MappedCredentialChanges.Values.Count > 0)
            {
                CredentialTask.Tasks["Custom"].AddRange(
                    await
                        ServiceManager.BuildServiceTasksFromApplicationsAsync(serviceApps,
                            MappedCredentialChanges.Values.ToList()));
            }

            // IF: FED -> Build task and get ADFS Nodes from platform for PS exec:
            return CredentialTask;
        }

    }
}
