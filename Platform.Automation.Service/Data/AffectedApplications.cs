﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class AffectedApplications
    {
        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("items")]
        public List<ApplicationItem> Items { get; set; }
    }

    public class ApplicationItem
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("application")]
        public Application Application { get; set; }

        [JsonProperty("components")]
        public Application Components { get; set; }

        [JsonProperty("dataPartitions")]
        public Application DataPartitions { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("resourceAllocation")]
        public ResourceAllocation ResourceAllocation { get; set; }

        [JsonProperty("stage")]
        public string Stage { get; set; }
    }

    public class ResourceAllocation
    {
        [JsonProperty("allocatedCpu")]
        public string AllocatedCpu { get; set; }

        [JsonProperty("allocatedMemory")]
        public string AllocatedMemory { get; set; }

        [JsonProperty("allocatedStorage")]
        public object AllocatedStorage { get; set; }
    }

    public class Application
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }
}
