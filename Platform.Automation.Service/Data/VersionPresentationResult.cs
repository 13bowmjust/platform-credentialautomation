﻿using Apprenda.Utility.Windows;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class VersionPresentationResult : IComponent
    {
        [JsonProperty("d")]
        public VersionPresentationResponse D { get; set; }

        [JsonIgnore]
        public VersionPresentation PresentationSla
        {
            get => new VersionPresentation()
            {
                Sla = new Sla()
                {
                    DeveloperId = D.DeveloperId,
                    Domain = D.Domain,
                    MinCount = D.MinimumInstanceCount.ToString(),
                    Password = D.Password,
                    UserAccount = D.UserAccount,
                    VersionId = D.VersionId
                }
            };
        }

        [JsonIgnore]
        public Credentials Credentials { get => new Apprenda.Utility.Windows.Credentials(D.Domain, D.UserAccount, D.Password); }

        [JsonIgnore]
        public string Name { get => D.Type; }

        public class VersionPresentationResponse
        {
            [JsonProperty("Domain")]
            public string Domain { get; set; }

            [JsonProperty("DeveloperId")]
            public string DeveloperId { get; set; }

            [JsonProperty("IsBlacklistedFromInstanceCounts")]
            public bool IsBlacklistedFromInstanceCounts { get; set; }

            [JsonProperty("MaximumInstanceCount")]
            public long MaximumInstanceCount { get; set; }

            [JsonProperty("MinimumInstanceCount")]
            public long MinimumInstanceCount { get; set; }

            [JsonProperty("Password")]
            public string Password { get; set; }

            [JsonProperty("ScalingType")]
            public long ScalingType { get; set; }

            [JsonProperty("__type")]
            public string Type { get; set; }

            [JsonProperty("UserAccount")]
            public string UserAccount { get; set; }

            [JsonProperty("VersionId")]
            public string VersionId { get; set; }
        }
    }
}
