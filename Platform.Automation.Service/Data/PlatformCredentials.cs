﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class PlatformCredentials
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
