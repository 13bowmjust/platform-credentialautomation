﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class VersionArtifactResult
    {
        [JsonProperty("d")]
        public VersionArtifactResponse D { get; set; }
    }

    public class VersionArtifactResponse
    {
        [JsonProperty("Page")]
        public long Page { get; set; }

        [JsonProperty("RecordsPerPage")]
        public long RecordsPerPage { get; set; }

        [JsonProperty("Results")]
        public List<ArtifactResult> Results { get; set; }

        [JsonProperty("TotalPages")]
        public long TotalPages { get; set; }

        [JsonProperty("TotalRecords")]
        public long TotalRecords { get; set; }

        [JsonProperty("__type")]
        public string Type { get; set; }
    }

    public class ArtifactResult
    {
        [JsonProperty("ApplicationAlias")]
        public string ApplicationAlias { get; set; }

        [JsonProperty("ApplicationDescription")]
        public string ApplicationDescription { get; set; }

        [JsonProperty("ApplicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("ApplicationName")]
        public string ApplicationName { get; set; }

        [JsonProperty("ArtifactId")]
        public string ArtifactId { get; set; }

        [JsonProperty("ArtifactName")]
        public string ArtifactName { get; set; }

        [JsonProperty("BundleName")]
        public string BundleName { get; set; }

        [JsonProperty("ComponentType")]
        public long ComponentType { get; set; }

        [JsonProperty("DeveloperAlias")]
        public string DeveloperAlias { get; set; }

        [JsonProperty("DeveloperId")]
        public string DeveloperId { get; set; }

        [JsonProperty("DeveloperName")]
        public string DeveloperName { get; set; }

        [JsonProperty("ImplementationGroupId")]
        public string ImplementationGroupId { get; set; }

        [JsonProperty("PolicyAllowsDeployment")]
        public bool PolicyAllowsDeployment { get; set; }

        [JsonProperty("PolicyCpuCores")]
        public long PolicyCpuCores { get; set; }

        [JsonProperty("PolicyCpuLimitInMegahertz")]
        public object PolicyCpuLimitInMegahertz { get; set; }

        [JsonProperty("PolicyDescription")]
        public string PolicyDescription { get; set; }

        [JsonProperty("PolicyId")]
        public string PolicyId { get; set; }

        [JsonProperty("PolicyIsActive")]
        public bool PolicyIsActive { get; set; }

        [JsonProperty("PolicyMemoryLimitInMegabytes")]
        public object PolicyMemoryLimitInMegabytes { get; set; }

        [JsonProperty("PolicyName")]
        public string PolicyName { get; set; }

        [JsonProperty("PolicyNotes")]
        public string PolicyNotes { get; set; }

        [JsonProperty("PolicyReplaced")]
        public bool PolicyReplaced { get; set; }

        [JsonProperty("PolicyType")]
        public long PolicyType { get; set; }

        [JsonProperty("PolicyUnitCost")]
        public object PolicyUnitCost { get; set; }

        [JsonProperty("PolicyUnitType")]
        public string PolicyUnitType { get; set; }

        [JsonProperty("PolicyVersionId")]
        public string PolicyVersionId { get; set; }

        [JsonProperty("ServiceBinary")]
        public string ServiceBinary { get; set; }

        [JsonProperty("ServiceHostingType")]
        public long ServiceHostingType { get; set; }

        [JsonProperty("ServiceRepositoryLocation")]
        public string ServiceRepositoryLocation { get; set; }

        [JsonProperty("VersionAlias")]
        public string VersionAlias { get; set; }

        [JsonProperty("VersionDescription")]
        public object VersionDescription { get; set; }

        [JsonProperty("VersionId")]
        public string VersionId { get; set; }

        [JsonProperty("VersionName")]
        public string VersionName { get; set; }

        [JsonProperty("VersionStage")]
        public string VersionStage { get; set; }
    }
}
