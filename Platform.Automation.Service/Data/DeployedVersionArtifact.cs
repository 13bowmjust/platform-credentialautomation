﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class DeployedVersionArtifact
    {
        [JsonProperty("artifactId")]
        public string ArtifactId { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }
    }
}
