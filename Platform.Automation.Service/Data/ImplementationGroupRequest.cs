﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class ImplementationGroupRequest
    {
        [JsonProperty("implementationGroupId")]
        public string ImplementationId { get; set; }
    }
}
