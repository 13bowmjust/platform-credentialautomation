﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class DeployedVersionArtifactResult
    {
        [JsonProperty("d")]
        public DeployedArtifactResponse D { get; set; }
    }

    public class DeployedArtifactResponse
    {
        [JsonProperty("Page")]
        public long Page { get; set; }

        [JsonProperty("RecordsPerPage")]
        public long RecordsPerPage { get; set; }

        [JsonProperty("Results")]
        public List<DeployedArtifactResult> Results { get; set; }

        [JsonProperty("TotalPages")]
        public long TotalPages { get; set; }

        [JsonProperty("TotalRecords")]
        public long TotalRecords { get; set; }

        [JsonProperty("__type")]
        public string Type { get; set; }
    }

    public class DeployedArtifactResult
    {
        [JsonProperty("ApplicationAlias")]
        public string ApplicationAlias { get; set; }

        [JsonProperty("ArtifactId")]
        public string ArtifactId { get; set; }

        [JsonProperty("ArtifactName")]
        public string ArtifactName { get; set; }

        [JsonProperty("AverageCpu")]
        public long AverageCpu { get; set; }

        [JsonProperty("AverageMemory")]
        public long AverageMemory { get; set; }

        [JsonProperty("ComponentType")]
        public long ComponentType { get; set; }

        [JsonProperty("DeployTime")]
        public string DeployTime { get; set; }

        [JsonProperty("DeployedArtifactId")]
        public long DeployedArtifactId { get; set; }

        [JsonProperty("DeployedPolicyCpuLimit")]
        public object DeployedPolicyCpuLimit { get; set; }

        [JsonProperty("DeployedPolicyDescription")]
        public string DeployedPolicyDescription { get; set; }

        [JsonProperty("DeployedPolicyId")]
        public string DeployedPolicyId { get; set; }

        [JsonProperty("DeployedPolicyMemoryLimit")]
        public object DeployedPolicyMemoryLimit { get; set; }

        [JsonProperty("DeployedPolicyName")]
        public string DeployedPolicyName { get; set; }

        [JsonProperty("DeveloperAlias")]
        public string DeveloperAlias { get; set; }

        [JsonProperty("HostCpuCores")]
        public long HostCpuCores { get; set; }

        [JsonProperty("HostId")]
        public long HostId { get; set; }

        [JsonProperty("HostName")]
        public string HostName { get; set; }

        [JsonProperty("HostTotalCpu")]
        public long HostTotalCpu { get; set; }

        [JsonProperty("HostTotalMemory")]
        public long HostTotalMemory { get; set; }

        [JsonProperty("HostType")]
        public long HostType { get; set; }

        [JsonProperty("InstanceId")]
        public string InstanceId { get; set; }

        [JsonProperty("IsPolicyApplicable")]
        public bool? IsPolicyApplicable { get; set; }

        [JsonProperty("LastEventTime")]
        public string LastEventTime { get; set; }

        [JsonProperty("LastEventType")]
        public string LastEventType { get; set; }

        [JsonProperty("LaunchPadUri")]
        public string LaunchPadUri { get; set; }

        [JsonProperty("ModelId")]
        public string ModelId { get; set; }

        [JsonProperty("PolicyCpuLimitInFractionOfCore")]
        public object PolicyCpuLimitInFractionOfCore { get; set; }

        [JsonProperty("PolicyCpuLimitInMegahertz")]
        public long PolicyCpuLimitInMegahertz { get; set; }

        [JsonProperty("PolicyDescription")]
        public string PolicyDescription { get; set; }

        [JsonProperty("PolicyId")]
        public string PolicyId { get; set; }

        [JsonProperty("PolicyMemoryLimitInMegabytes")]
        public long PolicyMemoryLimitInMegabytes { get; set; }

        [JsonProperty("PolicyName")]
        public string PolicyName { get; set; }

        [JsonProperty("PolicyVersionId")]
        public string PolicyVersionId { get; set; }

        [JsonProperty("ProcessId")]
        public long? ProcessId { get; set; }

        [JsonProperty("ServiceHostingType")]
        public long? ServiceHostingType { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }

        [JsonProperty("StorageQuotaSelectionId")]
        public long? StorageQuotaSelectionId { get; set; }

        [JsonProperty("VersionAlias")]
        public string VersionAlias { get; set; }
    }
}
