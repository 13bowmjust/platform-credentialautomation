﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class VersionPresentation
    {
        [JsonProperty("sla")]
        public Sla Sla { get; set; }
    }

    public class Sla
    {
        [JsonProperty("DeveloperId")]
        public string DeveloperId { get; set; }

        [JsonProperty("Domain")]
        public string Domain { get; set; }

        [JsonProperty("MinCount")]
        public string MinCount { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("UserAccount")]
        public string UserAccount { get; set; }

        [JsonProperty("VersionId")]
        public string VersionId { get; set; }
    }
}
