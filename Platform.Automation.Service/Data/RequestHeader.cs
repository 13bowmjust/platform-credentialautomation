﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class RequestHeader
    {
        public string Host { get; set; }

        public string Origin { get; set; }

        public string Referer { get; set; }

        [JsonProperty(PropertyName = "content-type")]
        public string ContentType { get; set; }

        public string AuthenticationToken { get; set; }
    }
}
