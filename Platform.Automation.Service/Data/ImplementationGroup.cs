﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class ImplementationGroup
    {
        [JsonProperty("group")]
        public Group Group { get; set; }
    }

    public class Group
    {
        [JsonProperty("Domain")]
        public string Domain { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("InPlaceRecoverable")]
        public bool InPlaceRecoverable { get; set; }

        [JsonProperty("MaximumInstanceCount")]
        public string MaximumInstanceCount { get; set; }

        [JsonProperty("MinimumInstanceCount")]
        public string MinimumInstanceCount { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }
    }
}

