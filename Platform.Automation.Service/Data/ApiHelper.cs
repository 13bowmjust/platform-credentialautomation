﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apprenda.Utility;
using Newtonsoft.Json;
using RestSharp;
using Apprenda.SaaSGrid.Topology;

namespace Platform.Automation.Service.Data
{
    public enum APIEndpoints
    {
        SocApplications,
        SocApplicationVersions,
        SocWorkloadsGet,
        SocWorkloadsUpdate,
        SocWorkloadsRemove,
        SocNodeStateGet,
        SocNodeStateSet,
        SocRegistryGet,
        SocRegistrySet,
        Authentication,
        ServiceCatalogGet,
        ServiceCatalogUpdate,
        VersionPresentationGet,
        VersionPresentationUpdate,
        ApplicationInformation
    };

    public class ApiHelper
    {

        // Filter used to find ONLY the ACP applications: [DeveloperID] OF [VersionArtifact]
        public static readonly SaaSGridGuid ApprendaId = new SaaSGridGuid("00000000-0000-4000-0000-000000000001");
        public static readonly string ApprendaDevName = "Apprenda Inc";

        public static IRestRequest GetApiEndpoint(APIEndpoints type,
            string versionId = "",
            string appAlias = "",
            string nodeRole = "",
            string regName = "")
        {
            var url = "";
            var method = Method.GET;

            switch (type)
            {
                case APIEndpoints.SocNodeStateGet:
                    url = $"/soc/api/v1/nodes/{nodeRole}?pagesize=1000";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocApplications:
                    url = "/soc/api/v1/applications?pagesize=1000";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocApplicationVersions:
                    url = $"/soc/api/v1/applications/{appAlias}/versions";
                    method = Method.GET;
                    break;
                case APIEndpoints.Authentication:
                    url = "/authentication/api/v1/sessions/soc";
                    method = Method.POST;
                    break;
                case APIEndpoints.ApplicationInformation:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionArtifacts";
                    method = Method.POST;
                    break;
                case APIEndpoints.ServiceCatalogGet:
                    url = "/soc/ServiceCatalogService.asmx/GetImplementationGroup";
                    method = Method.POST;
                    break;
                case APIEndpoints.ServiceCatalogUpdate:
                    url = "/soc/ServiceCatalogService.asmx/UpdateImplementationGroup";
                    method = Method.POST;
                    break;
                case APIEndpoints.VersionPresentationGet:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionPresentation";
                    method = Method.POST;
                    break;
                case APIEndpoints.VersionPresentationUpdate:
                    url = "/soc/ApplicationInformationService.asmx/UpdatePresentationSla";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsGet:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionDeployedArtifacts";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsUpdate:
                    url = "/soc/TopologyService.asmx/DeployWorkload";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsRemove:
                    url = "/soc/TopologyService.asmx/UndeployInstance";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocRegistryGet:
                    url = $"/soc/api/v1/registry/{regName}";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocRegistrySet:
                    url = $"/soc/api/v1/registry/{regName}";
                    method = Method.PUT;
                    break;
            }

            return new RestRequest(url, method);
        }

        public static IRestRequest GetApiEndpointRequest(APIEndpoints type, params string[] namedOptions)
        {
            var url = "";
            var method = Method.GET;

            switch (type)
            {
                case APIEndpoints.SocNodeStateGet:
                    url = $"/soc/api/v1/nodes/{namedOptions[0]}?pagesize=1000";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocApplications:
                    url = "/soc/api/v1/applications?pagesize=1000";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocApplicationVersions:
                    url = $"/soc/api/v1/applications/{namedOptions[0]}/versions";
                    method = Method.GET;
                    break;
                case APIEndpoints.Authentication:
                    url = "/authentication/api/v1/sessions/soc";
                    method = Method.POST;
                    break;
                case APIEndpoints.ApplicationInformation:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionArtifacts";
                    method = Method.POST;
                    break;
                case APIEndpoints.ServiceCatalogGet:
                    url = "/soc/ServiceCatalogService.asmx/GetImplementationGroup";
                    method = Method.POST;
                    break;
                case APIEndpoints.ServiceCatalogUpdate:
                    url = "/soc/ServiceCatalogService.asmx/UpdateImplementationGroup";
                    method = Method.POST;
                    break;
                case APIEndpoints.VersionPresentationGet:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionPresentation";
                    method = Method.POST;
                    break;
                case APIEndpoints.VersionPresentationUpdate:
                    url = "/soc/ApplicationInformationService.asmx/UpdatePresentationSla";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsGet:
                    url = "/soc/ApplicationInformationService.asmx/GetVersionDeployedArtifacts";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsUpdate:
                    url = "/soc/TopologyService.asmx/DeployWorkload";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocWorkloadsRemove:
                    url = "/soc/TopologyService.asmx/UndeployInstance";
                    method = Method.POST;
                    break;
                case APIEndpoints.SocRegistryGet:
                    url = $"/soc/api/v1/registry/{namedOptions[0]}";
                    method = Method.GET;
                    break;
                case APIEndpoints.SocRegistrySet:
                    url = $"/soc/api/v1/registry/{namedOptions[0]}";
                    method = Method.PUT;
                    break;
            }

            return new RestRequest(url, method);
        }


        public static IRestRequest EndApiSessionRequest(string authToken)
        {
            return new RestRequest($"/authentication/api/v1/sessions/soc/{authToken}", Method.DELETE);
        }

        public static void LogJson(object output, string name)
        {
            using (StreamWriter sw = File.AppendText($"{name}.json"))
            {
                JsonSerializer js = new JsonSerializer();
                js.Formatting = Formatting.Indented;

                js.Serialize(sw, output);
            }
        }
    }
}
