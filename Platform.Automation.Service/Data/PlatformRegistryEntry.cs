﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class PlatformRegistryEntry
    {
        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("isEncrypted")]
        public bool IsEncrypted { get; set; }

        [JsonProperty("isReadOnly")]
        public bool IsReadOnly { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
