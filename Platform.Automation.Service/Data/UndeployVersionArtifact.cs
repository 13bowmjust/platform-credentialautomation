﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class UndeployVersionArtifact
    {
        [JsonProperty("instanceId")]
        public string InstanceId { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }
    }

}
