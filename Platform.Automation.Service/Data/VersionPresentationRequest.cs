﻿using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class VersionPresentationRequest
    {
        [JsonProperty("developerId")]
        public string DeveloperId { get; set; }

        [JsonProperty("versionId")]
        public string VersionId { get; set; }
    }
}
