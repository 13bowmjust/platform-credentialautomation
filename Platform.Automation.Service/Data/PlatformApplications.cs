﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class PlatformApplications
    {
        [JsonProperty("currentPage")]
        public long CurrentPage { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("items")]
        public List<PlatformItem> Items { get; set; }

        [JsonProperty("nextPage")]
        public NextPage NextPage { get; set; }

        [JsonProperty("pageSize")]
        public long PageSize { get; set; }

        [JsonProperty("previousPage")]
        public NextPage PreviousPage { get; set; }

        [JsonProperty("totalItems")]
        public long TotalItems { get; set; }

        [JsonProperty("totalPages")]
        public long TotalPages { get; set; }
    }

    public class PlatformItem
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("applicationServices")]
        public string ApplicationServices { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("developerName")]
        public string DeveloperName { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("versions")]
        public NextPage Versions { get; set; }
    }

    public class NextPage
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }
}
