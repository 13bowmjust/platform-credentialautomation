﻿using Newtonsoft.Json;
using RestSharp.Serializers;

namespace Platform.Automation.Service.Data
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VersionArtifact
    {
        [JsonProperty(PropertyName = "nd")]
        [SerializeAs(Name = "nd")]
        public long Nd { get; set; }

        [JsonProperty(PropertyName = "page")]
        [SerializeAs(Name = "page")]
        public long Page { get; set; }

        [JsonProperty(PropertyName = "rows")]
        [SerializeAs(Name = "rows")]
        public long Rows { get; set; }

        [JsonProperty(PropertyName = "_search")]
        [SerializeAs(Name = "_search")]
        public bool Search { get; set; }

        [JsonProperty(PropertyName = "searchColumn")]
        [SerializeAs(Name = "searchColumn")]
        public string SearchColumn { get; set; }

        [JsonProperty(PropertyName = "searchTerm")]
        [SerializeAs(Name = "searchTerm")]
        public string SearchTerm { get; set; }

        [JsonProperty(PropertyName = "sidx")]
        [SerializeAs(Name = "sidx")]
        public string Sidx { get; set; }

        [JsonProperty(PropertyName = "sord")]
        [SerializeAs(Name = "sord")]
        public string Sord { get; set; }

        [JsonProperty(PropertyName = "versionId")]
        [SerializeAs(Name = "versionId")]
        public string VersionId { get; set; }
    }
}
