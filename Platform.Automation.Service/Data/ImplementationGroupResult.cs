﻿using Apprenda.Utility.Windows;
using Newtonsoft.Json;

namespace Platform.Automation.Service.Data
{
    public class ImplementationGroupResult : IComponent
    {
        [JsonProperty("d")]
        public ImplGroupResponse D { get; set; }

        [JsonIgnore]
        public ImplementationGroup CurrentImplementationGroup
        {
            get => new ImplementationGroup()
            {
                Group = new Group()
                {
                    Domain = D.Payload.Domain,
                    Id = D.Payload.Id,
                    InPlaceRecoverable = D.Payload.IsInPlaceRecoverable,
                    MaximumInstanceCount = D.Payload.MaximumInstanceCount.ToString(),
                    MinimumInstanceCount = D.Payload.MinimumInstanceCount.ToString(),
                    Password = D.Payload.Password,
                    Username = D.Payload.UserName
                }
            };
        }

        [JsonIgnore]
        public Credentials Credentials { get => new Credentials(D.Payload.Domain, D.Payload.UserName, D.Payload.Password); }

        [JsonIgnore]
        public string Name { get => D.Payload.Name; }
    }

    public class ImplGroupResponse
    {
        [JsonProperty("ErrorMessage")]
        public object ErrorMessage { get; set; }

        [JsonProperty("HasError")]
        public bool HasError { get; set; }

        [JsonProperty("HasWarning")]
        public bool HasWarning { get; set; }

        [JsonProperty("Payload")]
        public ImplGroupPayload Payload { get; set; }

        [JsonProperty("WarningMessage")]
        public object WarningMessage { get; set; }
    }

    public class ImplGroupPayload
    {
        [JsonProperty("ApplicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("ApplicationName")]
        public string ApplicationName { get; set; }

        [JsonProperty("BinaryName")]
        public string BinaryName { get; set; }

        [JsonProperty("Domain")]
        public string Domain { get; set; }

        [JsonProperty("HostingType")]
        public long HostingType { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("IsBlacklistedFromInstanceCounts")]
        public bool IsBlacklistedFromInstanceCounts { get; set; }

        [JsonProperty("IsInPlaceRecoverable")]
        public bool IsInPlaceRecoverable { get; set; }

        [JsonProperty("MaximumInstanceCount")]
        public long MaximumInstanceCount { get; set; }

        [JsonProperty("MinimumInstanceCount")]
        public long MinimumInstanceCount { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("ProviderId")]
        public string ProviderId { get; set; }

        [JsonProperty("ProviderName")]
        public string ProviderName { get; set; }

        [JsonProperty("ScalingType")]
        public long ScalingType { get; set; }

        [JsonProperty("UserName")]
        public string UserName { get; set; }

        [JsonProperty("VersionId")]
        public string VersionId { get; set; }

        [JsonProperty("VersionName")]
        public string VersionName { get; set; }
    }
}
