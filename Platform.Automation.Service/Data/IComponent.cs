﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apprenda.Utility.Windows;

namespace Platform.Automation.Service.Data
{
    public interface IComponent
    {

        string Name { get;}
        Credentials Credentials { get; }

    }
}
