﻿using System;
using System.Collections.Generic;
using System.Linq;
using Apprenda.Utility.Windows;
using Platform.Automation.Service.Data;

namespace Platform.Automation.Service.Tasks
{
    public class ServiceTask
    {
        public VersionArtifactResult VersionArtifactResult { get; set; }
        public DeployedVersionArtifactResult DeployedVersionArtifactResults { get; set; }
        public List<ImplementationGroupResult> ImplementationGroupResults { get; set; }
        public AffectedApplications AffectedApplications { get; set; }
        public List<VersionPresentationResult> VersionPresentationResults { get; set; }
        public DeployedVersionArtifactResult DeployedArtifactResult { get; set; }
        public List<Credentials> ChangedCredentials { get; set; }

        public ServiceTask()
        {
            ImplementationGroupResults = new List<ImplementationGroupResult>();
            VersionPresentationResults = new List<VersionPresentationResult>();
            VersionArtifactResult = new VersionArtifactResult();
            DeployedArtifactResult = new DeployedVersionArtifactResult();
        }

        public string ApplicationName { get; set; }
        public IEnumerable<string> ApplicationVersions => VersionArtifactResult.D.Results.Select(x => x.VersionId);
        public IEnumerable<string> DeployedComponentIds => VersionArtifactResult.D.Results.Select(x => x.ArtifactId);

        public IEnumerable<string> DeployedInstanceIds
            => DeployedVersionArtifactResults.D.Results.Select(x => x.InstanceId);

        public IEnumerable<Credentials> CurrentServiceCredentials
        {
            get { return ImplementationGroupResults.Select(x => x.Credentials); }
        }

        public bool UsesDefaultCredentials
        {
            get
            {
                return (ImplementationGroupResults.Any(x => x.Credentials.UserName == null) &&
                    VersionPresentationResults.Any(x => x.Credentials.UserName == null));
            }
        }
    }
}
