﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Platform.Automation.Service.Data;
using RestSharp;

namespace Platform.Automation.Service
{
    public class PlatformAgent
    {

        #region Fields:

        public string PlatformUrl { get; set; }
        public PlatformCredentials PlatformCredentials { get; set; }
        public string AuthenticationToken { get; set; }
        public RestClient PlatformClient { get; set; }

        #endregion

        public PlatformAgent()
        {

        }

        public PlatformAgent(string url, string username, string password)
        {
            PlatformUrl = url;
            PlatformCredentials = new PlatformCredentials() { Username = username, Password = password };

            sv();
        }

        ~PlatformAgent()
        {
            //PlatformClient.ExecuteTaskAsync(ApiHelper.EndApiSessionRequest(AuthenticationToken));
        }

        private IRestRequest CreateServiceRequest(APIEndpoints type, string versionId, object body = null)
        {
            // Get the requested API endpoint for the Request:
            var request = ApiHelper.GetApiEndpoint(type, versionId);

            // Add required Headers:
            request.AddHeader("ApprendaSessionToken", AuthenticationToken);
            request.AddHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Host", PlatformUrl.Remove(0, 8));
            request.AddHeader("Origin", PlatformUrl);
            request.AddHeader("Referer", $"{PlatformUrl}/soc/Applications/Versions/?vid={versionId}");

            var serializedBody = JsonConvert.SerializeObject(body);

            request.AddParameter("application/json", serializedBody, ParameterType.RequestBody);

            return request;
        }

        private IRestRequest CreateApiRequest(APIEndpoints type, string appAlias = "", object body = null)
        {
            var request = ApiHelper.GetApiEndpoint(type, appAlias: appAlias);

            request.AddHeader("Content-Type", "application/json");

            if (body != null)
            {
                var serializedBody = JsonConvert.SerializeObject(body);
                request.AddParameter("application/json", serializedBody, ParameterType.RequestBody);
            }

            if (!string.IsNullOrEmpty(AuthenticationToken))
            {
                request.AddHeader("ApprendaSessionToken", AuthenticationToken);
            }

            return request;
        }

        private IRestRequest CreateApiRequest(APIEndpoints type, object body = null, params string[] namedOptions)
        {
            var request = ApiHelper.GetApiEndpointRequest(type, namedOptions);

            request.AddHeader("Content-Type", "application/json");

            if (body != null)
            {
                var serializedBody = JsonConvert.SerializeObject(body);
                request.AddParameter("application/json", serializedBody, ParameterType.RequestBody);
            }

            if (!string.IsNullOrEmpty(AuthenticationToken))
            {
                request.AddHeader("ApprendaSessionToken", AuthenticationToken);
            }

            return request;
        }

        public IRestResponse ExecuteServiceRequest(APIEndpoints type, string versionId, object body = null)
        {
            var request = CreateServiceRequest(type, versionId, body);

            var result = PlatformClient.Execute(request);

            return result;
        }

        public async Task<IRestResponse> ExecuteServiceRequestAsync(APIEndpoints type, string versionId = "", object body = null)
        {
            var request = CreateServiceRequest(type, versionId, body);

            var result = await PlatformClient.ExecuteTaskAsync(request);

            return result;
        }

        public IRestResponse ExecuteApiRequest(APIEndpoints type, string appAlias = "", object body = null)
        {
            var request = CreateApiRequest(type, appAlias, body);

            var result = PlatformClient.Execute(request);

            return result;
        }

        public async Task<IRestResponse> ExecuteApiRequestAsync(APIEndpoints type, string appAlias = "", object body = null)
        {
            var request = CreateApiRequest(type, appAlias, body);

            var result = await PlatformClient.ExecuteTaskAsync(request);

            return result;
        }

        public async Task<IRestResponse> ExecuteApiRequestAsync(APIEndpoints type, object body = null, params string[] namedOptions)
        {
            var request = CreateApiRequest(type, body, namedOptions);

            var result = await PlatformClient.ExecuteTaskAsync(request);

            return result;
        }

        public async Task<PlatformRegistryEntry> GetPlatformRegistryEntry(string name)
        {
            var entryJson = await ExecuteApiRequestAsync(APIEndpoints.SocRegistryGet, namedOptions: name);

            return JsonConvert.DeserializeObject<PlatformRegistryEntry>(entryJson.Content);
        }

        public async Task<IRestResponse> SetPlatformRegistryEntry(PlatformRegistryEntry updatedEntry)
        {
            var response = await ExecuteApiRequestAsync(APIEndpoints.SocRegistrySet, updatedEntry, updatedEntry.Name);

            return response;
        }

        public async Task<IRestResponse> AuthenticateAsync()
        {
            PlatformClient = new RestClient(PlatformUrl);

            var request = CreateApiRequest(APIEndpoints.Authentication,
                body: PlatformCredentials);

            var result = await PlatformClient.ExecutePostTaskAsync(request);

            if (result.ResponseStatus == ResponseStatus.Completed
                && result.StatusCode == HttpStatusCode.Created)
            {
                dynamic tokenHeader = JsonConvert.DeserializeObject(result.Content);

                AuthenticationToken = tokenHeader.apprendaSessionToken.ToString();
            }

            return result;
        }

        private static void sv()
        {
            ServicePointManager.ServerCertificateValidationCallback = ic;
        }

        private static bool ic(object o, X509Certificate cert, X509Chain c, SslPolicyErrors s)
        {
            return true;
        }

    }
}
