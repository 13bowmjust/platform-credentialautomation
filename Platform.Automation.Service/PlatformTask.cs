﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Automation.Service.Data;
using Platform.Automation.Service.Tasks;
using Credentials = Apprenda.Utility.Windows.Credentials;

namespace Platform.Automation.Service
{
    public class PlatformTask
    {
        public List<Credentials> ChangedCredentials { get; set; } 
        public Dictionary<string, List<ServiceTask>> Tasks { get; set; }

        public PlatformTask()
        {
            Tasks = new Dictionary<string, List<ServiceTask>>();
            Tasks.Add("Default", new List<ServiceTask>());
            Tasks.Add("Custom", new List<ServiceTask>());
            Tasks.Add("Apprenda", new List<ServiceTask>());

            ChangedCredentials = new List<Credentials>();
        }
    }
}
