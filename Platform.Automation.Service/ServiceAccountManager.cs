﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Apprenda.Utility;
using Apprenda.Utility.Extensions;
using Newtonsoft.Json;
using Platform.Automation.Service.Data;
using Platform.Automation.Service.Tasks;
using RestSharp;
using Credentials = Apprenda.Utility.Windows.Credentials;

namespace Platform.Automation.Service
{
    public class ServiceAccountManager
    {

        public PlatformAgent Agent { get; set; }

        public ServiceAccountManager()
        {
            Agent = new PlatformAgent();
        }

        public async Task<IRestResponse> Connect(string platformUrl, string username, string password)
        {
            Agent = new PlatformAgent(platformUrl, username, password);

            var result = await Agent.AuthenticateAsync();

            return result;
        }

        #region Synchronous Functions:

        public PlatformApplications GetApplications()
        {
            var response = Agent.ExecuteApiRequest(APIEndpoints.SocApplications);

            return JsonConvert.DeserializeObject<PlatformApplications>(response.Content);
        }

        public AffectedApplications GetApplicationVersionInfoByAlias(string appAlias)
        {
            var response = Agent.ExecuteApiRequest(APIEndpoints.SocApplicationVersions, appAlias);

            return JsonConvert.DeserializeObject<AffectedApplications>(response.Content);
        }

        public VersionArtifactResult GetVersionArtifactById(string versionId)
        {
            var artifactBody = new VersionArtifact()
            {
                Nd = 1509377032091,
                Page = 1,
                Rows = 1000,
                Search = false,
                SearchColumn = "ArtifactName",
                SearchTerm = "",
                Sidx = "",
                Sord = "asc",
                VersionId = versionId
            };

            var response = Agent.ExecuteServiceRequest(APIEndpoints.ApplicationInformation, versionId, artifactBody);

            return JsonConvert.DeserializeObject<VersionArtifactResult>(response.Content);
        }

        public ImplementationGroupResult GetImplementationGroupById(string groupId, string versionId)
        {
            var impRequestBody = new ImplementationGroupRequest() { ImplementationId = groupId };

            var response = Agent.ExecuteServiceRequest(APIEndpoints.ServiceCatalogGet, versionId, impRequestBody);

            return JsonConvert.DeserializeObject<ImplementationGroupResult>(response.Content);
        }

        public VersionPresentationResult GetVersionPresentationByIds(string devId,
            string versionId)
        {
            var verPresentationBody = new VersionPresentationRequest()
            {
                DeveloperId = devId,
                VersionId = versionId
            };

            var response = Agent.ExecuteServiceRequest(APIEndpoints.VersionPresentationGet, versionId, verPresentationBody);

            return JsonConvert.DeserializeObject<VersionPresentationResult>(response.Content);
        }

        public string UpdateImplementationGroup(ImplementationGroup updatedGroup, string versionId)
        {
            var response = Agent.ExecuteServiceRequest(APIEndpoints.ServiceCatalogUpdate, versionId, updatedGroup);

            return "Successful";
        }

        public string UpdateVersionPresentation(VersionPresentation updatedPresentation, string versionId)
        {
            var response = Agent.ExecuteServiceRequest(APIEndpoints.VersionPresentationUpdate, versionId, updatedPresentation);

            return "Successful";
        }

        #endregion

        #region Asynchronous Functions:

        /// <summary>
        /// This Asynchronous method is used to retrieve all ACP Administrator related credentials
        /// from the specified platform when the ServiceAccountManager Connect().
        /// todo: this could be simplifed into another function that takes string[] params.
        /// </summary>
        /// <returns>List of Credentials used by platform for services.</returns>
        public async Task<List<Credentials>> GetPlatformAdminCredentialsAsync()
        {
            var creds = new List<Credentials>();

            // GET: SystemAdministrator Credentials from platform registry:
            var sysAdminDomain = await Agent.GetPlatformRegistryEntry("SystemAdministratorDomain");
            var sysAdminUsername = await Agent.GetPlatformRegistryEntry("SystemAdministratorUsername");
            var sysAdminPassword = await Agent.GetPlatformRegistryEntry("SystemAdministratorPassword");

            // Add the SystemAdministrator Credentials to our list:
            creds.Add(new Credentials(sysAdminDomain.Value, sysAdminUsername.Value, sysAdminPassword.Value ?? ""));

            // GET: SaaSGridServiceAccount Credentials from platform registry: (OPTIONAL based on username value)
            var saasGridServiceUsername = await Agent.GetPlatformRegistryEntry("SaaSGridDefaultServiceAccountUsername");
            if (!string.IsNullOrEmpty(saasGridServiceUsername.Value))
            {
                var saasGridServiceDomain = await Agent.GetPlatformRegistryEntry("SaaSGridDefaultServiceAccountDomain");
                var saasGridServicePassword = await Agent.GetPlatformRegistryEntry("SaaSGridDefaultServiceAccountPassword");

                creds.Add(new Credentials(saasGridServiceDomain.Value, saasGridServiceUsername.Value,
                    saasGridServicePassword.Value ?? ""));
            }

            // GET: SaaSGridSSystem Credentials from platform Registry: (OPTIONAL based on username value)
            var saasGridSystemUsername = await Agent.GetPlatformRegistryEntry("SaaSGridSystemUserName");
            if (!string.IsNullOrEmpty(saasGridSystemUsername.Value))
            {
                var saasGridSystemDomain = await Agent.GetPlatformRegistryEntry("SaaSGridSystemDomain");
                var saasGridSystemPassword = await Agent.GetPlatformRegistryEntry("SaaSGridSystemPassword");

                creds.Add(new Credentials(saasGridSystemDomain.Value, saasGridSystemUsername.Value,
                    saasGridSystemPassword.Value ?? ""));
            }

            return creds;
        }

        public async Task<string> SetPlatformAdminCredentialsAsync(List<Credentials> updatedCredentials)
        {
            var oldCredentials = await GetPlatformAdminCredentialsAsync();
            IRestResponse saasDefaultResponse = new RestResponse(), saasSystemResponse = new RestResponse();

            // 1.) SystemAdministrator Credentials:
            var sysAdminCreds = updatedCredentials.Find(x => x.UserName == oldCredentials[0].UserName);
            var sysAdminEntry = new PlatformRegistryEntry() { Name = "SystemAdministratorPassword", Value = sysAdminCreds.Password, IsEncrypted = true };
            var sysAdminResonse = await Agent.SetPlatformRegistryEntry(sysAdminEntry);

            if (oldCredentials.Count > 1)
            {
                // 2.) SaaSGridDefault
                var saasDefaultCreds = updatedCredentials.Find(x => x.UserName == oldCredentials[1].UserName);
                var saasDefaultEntry = new PlatformRegistryEntry() { Name = "SaaSGridDefaultServiceAccountPassword", Value = saasDefaultCreds.Password };
                saasDefaultResponse = await Agent.SetPlatformRegistryEntry(saasDefaultEntry);
            }

            if (oldCredentials.Count > 2)
            {
                // 2.) SaaSGridSystem
                var saasSystemCreds = updatedCredentials.Find(x => x.UserName == oldCredentials[1].UserName);
                var saasSystemEntry = new PlatformRegistryEntry() { Name = "SaaSGridSystemPassword", Value = saasSystemCreds.Password };
                saasSystemResponse = await Agent.SetPlatformRegistryEntry(saasSystemEntry);
            }

            return
                $"Results: {sysAdminResonse.StatusDescription}\r\n{saasDefaultResponse.StatusDescription}\r\n{saasSystemResponse.StatusDescription}";
        }

        public async Task<PlatformApplications> GetApplicationsAsync()
        {
            var response = await Agent.ExecuteApiRequestAsync(APIEndpoints.SocApplications);

            return JsonConvert.DeserializeObject<PlatformApplications>(response.Content);
        }

        public async Task<AffectedApplications> GetApplicationVersionInfoByAliasAsync(string appAlias)
        {
            var response = await Agent.ExecuteApiRequestAsync(APIEndpoints.SocApplicationVersions, appAlias);

            return JsonConvert.DeserializeObject<AffectedApplications>(response.Content);
        }

        public async Task<VersionArtifactResult> GetVersionArtifactByIdAsync(string versionId)
        {
            var artifactBody = new VersionArtifact()
            {
                Nd = 1509377032091,
                Page = 1,
                Rows = 1000,
                Search = false,
                SearchColumn = "ArtifactName",
                SearchTerm = "",
                Sidx = "",
                Sord = "asc",
                VersionId = versionId
            };

            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.ApplicationInformation, versionId, artifactBody);

            return JsonConvert.DeserializeObject<VersionArtifactResult>(response.Content);
        }

        public async Task<ImplementationGroupResult> GetImplementationGroupByIdAsync(string groupId, string versionId)
        {
            var impRequestBody = new ImplementationGroupRequest() { ImplementationId = groupId };

            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.ServiceCatalogGet, versionId, impRequestBody);

            return JsonConvert.DeserializeObject<ImplementationGroupResult>(response.Content);
        }

        public async Task<VersionPresentationResult> GetVersionPresentationByIdsAsync(string devId,
            string versionId)
        {
            var verPresentationBody = new VersionPresentationRequest()
            {
                DeveloperId = devId,
                VersionId = versionId
            };

            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.VersionPresentationGet, versionId, verPresentationBody);

            return JsonConvert.DeserializeObject<VersionPresentationResult>(response.Content);
        }

        public async Task<string> UpdateImplementationGroupAsync(ImplementationGroup updatedGroup)
        {
            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.ServiceCatalogUpdate, body: updatedGroup);

            return response.StatusCode.ToString();
        }

        public async Task<string> UpdateVersionPresentationAsync(VersionPresentation updatedPresentation)
        {
            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.VersionPresentationUpdate, body: updatedPresentation);

            return response.StatusCode.ToString();
        }

        public async Task<DeployedVersionArtifactResult> GetDeployedVersionArtifactByIdAsync(string versionId)
        {
            var artifactBody = new VersionArtifact()
            {
                Nd = 1509377032091,
                Page = 1,
                Rows = 1000,
                Search = false,
                SearchColumn = "ArtifactName",
                SearchTerm = "",
                Sidx = "",
                Sord = "asc",
                VersionId = versionId
            };

            var response = await Agent.ExecuteServiceRequestAsync(APIEndpoints.SocWorkloadsGet, versionId, artifactBody);

            return JsonConvert.DeserializeObject<DeployedVersionArtifactResult>(response.Content);
        }

        public async Task<string> DeployVersionArtifactAsync(DeployedVersionArtifact deployedArtifact)
        {
            var taskResponse = "";

            var response =
                await Agent.ExecuteServiceRequestAsync(APIEndpoints.SocWorkloadsUpdate, body: deployedArtifact);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                taskResponse = $"Success {response.StatusDescription}";
            }
            else
            {
                taskResponse = $"Failed: {response.ErrorMessage}";
            }

            return taskResponse;
        }

        public async Task<string> UndeployVersionArtifactAsync(UndeployVersionArtifact undeployArtifact)
        {
            var taskResponse = "";

            var response =
                await Agent.ExecuteServiceRequestAsync(APIEndpoints.SocWorkloadsRemove, body: undeployArtifact);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                taskResponse = $"Success {response.StatusDescription}";
            }
            else
            {
                taskResponse = $"Failed: {response.ErrorMessage}";
            }

            return taskResponse;
        }

        private async Task<IList<ImplementationGroupResult>> GetAffectedServicesAsync(IEnumerable<ArtifactResult> serviceArtifacts, List<Credentials> changedCredentials)
        {
            var results = new List<ImplementationGroupResult>();

            foreach (var service in serviceArtifacts)
            {
                var implementationGroup =
                    await GetImplementationGroupByIdAsync(service.ImplementationGroupId, service.VersionId);

                // VALIDATE: Service Credentials match a set of credentials that have changed:
                // If changedCredentials is null, then there is no checking for credentials - add the group.
                // If username is null, then the group is using Default Credentials - add the group.
                // If changeCredentials contains a current credential - add the group.
                if (changedCredentials == null || 
                    string.IsNullOrEmpty(implementationGroup.D.Payload.UserName) ||
                    CompareCredentials(changedCredentials, new List<Credentials> { implementationGroup.Credentials }))
                {
                    results.Add(implementationGroup);
                }
            }

            return results;
        }

        private async Task<IList<VersionPresentationResult>> GetAffectedUserInterfacesAsync(IEnumerable<ArtifactResult> presentationArtifacts, List<Credentials> changedCredentials)
        {
            var results = new List<VersionPresentationResult>();

            foreach (var presentation in presentationArtifacts)
            {
                var presentationSla =
                    await GetVersionPresentationByIdsAsync(presentation.DeveloperId, presentation.VersionId);

                // VALIDATE: Presentation Credentials match a set of credentials that have changed:
                if (changedCredentials == null || 
                    string.IsNullOrEmpty(presentationSla.D.UserAccount) ||
                    CompareCredentials(changedCredentials, new List<Credentials> { presentationSla.Credentials }))
                {
                    results.Add(presentationSla);
                }
            }

            return results;
        }

        public async Task<List<ServiceTask>> BuildServiceTasksFromApplicationsAsync(List<PlatformItem> applications, List<Credentials> credentialFilter = null, bool apprendaApps = false)
        {
            var tempTasks = new List<ServiceTask>();

            // IF ApprendaApps is true, filter to ensure the last apps are the router and catalog services:
            if (apprendaApps == true)
            {
                // Find the Apprenda Router & Application Catalog: (these need to be last)
                var indexRouter = applications.FindIndex(x => x.Alias == "cluster-manager");
                var indexCatalog = applications.FindIndex(x => x.Alias == "application-catalog");
                var indexLast = applications.Count - 1;

                // Set the needed applications to be last:
                var appLast = applications[indexLast];
                var appNextLast = applications[indexLast - 1];

                applications[indexLast - 1] = applications[indexCatalog];
                applications[indexLast] = applications[indexRouter];

                // Set the old indexed spots to the apps that got replaced:
                applications[indexCatalog] = appNextLast;
                applications[indexRouter] = appLast;

            }

            foreach (var app in applications)
            {
                var tempTask = new ServiceTask();

                tempTask.ApplicationName = app.Name;

                if (credentialFilter != null)
                {
                    tempTask.ChangedCredentials = credentialFilter;
                }

                // GET: Application Versions from Alias:
                var applicationVersions = await GetApplicationVersionInfoByAliasAsync(app.Alias);

                tempTask.AffectedApplications = applicationVersions;

                // GET: VersionArtifacts and DeployedVersionArtifacts:
                foreach (var version in applicationVersions.Items)
                {
                    // Used now to determine what application version components to filter through:
                    var versionArtifacts = await GetVersionArtifactByIdAsync(version.Id);

                    // This is used later when the tasks are executed:
                    tempTask.DeployedVersionArtifactResults = await GetDeployedVersionArtifactByIdAsync(version.Id);

                    // FILTER: unneeded results like: DB Components, etc.
                    var serviceArtifacts = versionArtifacts.D.Results.FindAll(x => x.ComponentType == 3);

                    // Filter and find services:
                    tempTask.ImplementationGroupResults.AddRange(await GetAffectedServicesAsync(serviceArtifacts, tempTask.ChangedCredentials));

                    // For NON-Apprenda apps -> Filter and find user interfaces:
                    if (apprendaApps == false)
                    {
                        // For any set of applications that aren't Apprenda Apps - Get the UI settings as well:
                        var presentationArtifacts = versionArtifacts.D.Results.FindAll(x => x.ComponentType == 1);

                        tempTask.VersionPresentationResults.AddRange(
                            await GetAffectedUserInterfacesAsync(presentationArtifacts, tempTask.ChangedCredentials));
                    }

                }

                // Verify that the task has any actionable items to complete:
                if (tempTask.ImplementationGroupResults.Count > 0 || tempTask.VersionPresentationResults.Count > 0)
                {
                    // Add the task to the list:
                    tempTasks.Add(tempTask);
                }
            }
            // Return the tasks:
            return tempTasks;
        }

        #endregion

        public static bool CompareCredentials(List<Credentials> credentials,
            List<Credentials> credentialsToCompare)
        {
            return credentials.Intersect(credentialsToCompare).Any();
        }

        //public string IdentifyChanges()
        //{
        //    var tasks = new List<ServiceTask>();

        //    var time = DateTime.Now.TimeOfDay;

        //    // Get all Applications:
        //    var applications = GetApplications();

        //    // LOGOUTPUT:
        //    //ApiHelper.LogJson(applications, "SOCApplications");

        //    // APPLICATIONS:
        //    foreach (var app in applications.Items)
        //    {
        //        var tempTask = new ServiceTask();

        //        // Get each application version information: (Versions)
        //        tempTask.ApplicationsToUpdate = GetApplicationVersionInfoByAlias(app.Alias);

        //        // LOGOUTPUT:
        //        //ApiHelper.LogJson(tempTask.ApplicationsToUpdate, "AppVersionInfo");

        //        // APPLICATION VERSIONS:
        //        foreach (var appVersion in tempTask.ApplicationsToUpdate.Items)
        //        {
        //            // Get VersionArtifacts: (Application Information)
        //            tempTask.VersionArtifactResult = GetVersionArtifactById(appVersion.Id);

        //            // LOGOUTPUT:
        //            //ApiHelper.LogJson(tempTask.VersionArtifactResult, "VersionArtifactInfo");

        //            // VERSION ARTIFACTS:
        //            foreach (var verArtifacts in tempTask.VersionArtifactResult.D.Results)
        //            {
        //                if (verArtifacts.ComponentType != 2)
        //                {
        //                    if (verArtifacts.ComponentType != 1)
        //                    {
        //                        // Get Implementation Groups: (Services)
        //                        var implGroup =
        //                            GetImplementationGroupById(verArtifacts.ImplementationGroupId, verArtifacts.VersionId);

        //                        // LOGOUTPUT:
        //                        //ApiHelper.LogJson(implGroup, "ImplementationGroupInfo");

        //                        if (implGroup.D.Payload.Domain != null)
        //                        {
        //                            if (
        //                                Task.ChangedCredentials.Exists(
        //                                    x => x.UserName == implGroup.CurrentCredentials.UserName))
        //                            {
        //                                // A Change was found:
        //                                tempTask.ImplementationGroupResults.Add(implGroup);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // Get Version Presentations: (UI)
        //                        var verPresentation =
        //                            GetVersionPresentationByIds(verArtifacts.DeveloperId, verArtifacts.VersionId);

        //                        // LOGOUTPUT:
        //                        //ApiHelper.LogJson(verPresentation, "VersionPresentationInfo");

        //                        if (verPresentation.D.Domain != null)
        //                        {
        //                            if (Task.ChangedCredentials.Exists(
        //                                x => x.UserName == verPresentation.CurrentCredentials.UserName))
        //                            {
        //                                // A Change was found:
        //                                tempTask.VersionPresentationResults.Add(verPresentation);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        // LOGOUTPUT:
        //        //ApiHelper.LogJson(tempTask, "FinalTaskOutputInfo");

        //        // Evaluate if any changes were found, if so then add to the list:
        //        if (tempTask.ImplementationGroupResults.Count > 0
        //            || tempTask.VersionPresentationResults.Count > 0)
        //        {
        //            tasks.Add(tempTask);
        //        }

        //    }

        //    // Add results to the primary PlatformTask:
        //    Task.Tasks = tasks;

        //    var endTime = DateTime.Now.TimeOfDay;


        //    return (endTime - time).ToString();
        //}

        //public async Task<string> IdentifyChangesAsync()
        //{
        //    var tasks = new List<ServiceTask>();

        //    var time = DateTime.Now.TimeOfDay;

        //    // Get all Applications: (Apply Filter! -> Admin Cred changes?)
        //    var applications = await GetApplicationsAsync();

        //    // LOGOUTPUT:
        //    //ApiHelper.LogJson(applications, "SOCApplications");


        //    // APPLICATIONS:
        //    foreach (var app in applications.Items)
        //    {
        //        var tempTask = new ServiceTask();

        //        // Get each application version information: (Versions)
        //        tempTask.ApplicationsToUpdate = await GetApplicationVersionInfoByAliasAsync(app.Alias);

        //        // LOGOUTPUT:
        //        //ApiHelper.LogJson(tempTask.ApplicationsToUpdate, "AppVersionInfo");

        //        // APPLICATION VERSIONS:
        //        foreach (var appVersion in tempTask.ApplicationsToUpdate.Items)
        //        {
        //            // Get VersionArtifacts: (Application Information)
        //            tempTask.VersionArtifactResult = await GetVersionArtifactByIdAsync(appVersion.Id);
        //            tempTask.DeployedArtifactResult = await GetDeployedVersionArtifactByIdAsync(appVersion.Id);

        //            // LOGOUTPUT:
        //            //ApiHelper.LogJson(tempTask.VersionArtifactResult, "VersionArtifactInfo");

        //            // VERSION ARTIFACTS:
        //            foreach (var verArtifacts in tempTask.VersionArtifactResult.D.Results)
        //            {
        //                if (verArtifacts.ComponentType != 2)
        //                {
        //                    if (verArtifacts.ComponentType != 1)
        //                    {
        //                        // Get Implementation Groups: (Services)
        //                        var implGroup =
        //                            await GetImplementationGroupByIdAsync(verArtifacts.ImplementationGroupId, verArtifacts.VersionId);

        //                        // LOGOUTPUT:
        //                        //ApiHelper.LogJson(implGroup, "ImplementationGroupInfo");

        //                        if (implGroup.D.Payload.Domain != null)
        //                        {
        //                            if (
        //                                Task.ChangedCredentials.Exists(
        //                                    x => x.UserName == implGroup.CurrentCredentials.UserName))
        //                            {
        //                                // A Change was found:
        //                                tempTask.ImplementationGroupResults.Add(implGroup);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // Get Version Presentations: (UI)
        //                        var verPresentation =
        //                            await GetVersionPresentationByIdsAsync(verArtifacts.DeveloperId, verArtifacts.VersionId);
        //                        verPresentation.D.DeveloperId = verArtifacts.DeveloperId;

        //                        // LOGOUTPUT:
        //                        //ApiHelper.LogJson(verPresentation, "VersionPresentationInfo");

        //                        if (verPresentation.D.Domain != null)
        //                        {
        //                            if (Task.ChangedCredentials.Exists(
        //                                x => x.UserName == verPresentation.CurrentCredentials.UserName))
        //                            {
        //                                // A Change was found:
        //                                tempTask.VersionPresentationResults.Add(verPresentation);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        // LOGOUTPUT:
        //        //ApiHelper.LogJson(tempTask, "FinalTaskOutputInfo");

        //        // Evaluate if any changes were found, if so then add to the list:
        //        if (tempTask.ImplementationGroupResults.Count > 0
        //            || tempTask.VersionPresentationResults.Count > 0)
        //        {
        //            tasks.Add(tempTask);
        //        }

        //    }

        //    // Add results to the primary PlatformTask:
        //    Task.Tasks = tasks;

        //    var endTime = DateTime.Now.TimeOfDay;

        //    return (endTime - time).ToString();
        //}

        //public async Task<string> ImplementChangesAsync()
        //{
        //    var startTime = DateTime.Now.TimeOfDay;

        //    foreach (var task in Task.Tasks)
        //    {
        //        // Update the Services:
        //        foreach (var service in task.ImplementationGroupResults)
        //        {
        //            // Get the Service Group:
        //            var serviceSla = service.CurrentImplementationGroup;
        //            serviceSla.Group.MinimumInstanceCount = "1";
        //            // Modify the password:

        //            // Update:
        //            var response = await UpdateImplementationGroupAsync(serviceSla);
        //        }

        //        // Update the UIs:
        //        foreach (var ui in task.VersionPresentationResults)
        //        {
        //            // Get the UI Group:
        //            var presentationSla = new VersionPresentation()
        //            {

        //                Sla = new Sla()
        //                {
        //                    Domain = ui.D.Domain,
        //                    UserAccount = ui.D.UserAccount,
        //                    Password = ui.D.Password,
        //                    DeveloperId = ui.D.DeveloperId,
        //                    MinCount = "1",
        //                    VersionId = ui.D.VersionId
        //                }
        //            };
        //            // Modify the password:

        //            // Update:
        //            var response = await UpdateVersionPresentationAsync(presentationSla);
        //        }

        //        //Apply workload changes:
        //        foreach (var workload in task.DeployedArtifactResult.D.Results)
        //        {
        //            //TEMP!!!!FIXME:REMOVEME
        //            if (workload.ApplicationAlias != "application-catalog" ||
        //                workload.ApplicationAlias != "cluster-manager")
        //            {
        //                var newLoad = new DeployedVersionArtifact()
        //                {
        //                    Server = workload.HostName,
        //                    ArtifactId = workload.ArtifactId
        //                };

        //                var oldLoad = new UndeployVersionArtifact()
        //                {
        //                    Server = workload.HostName,
        //                    InstanceId = workload.InstanceId
        //                };

        //                // Deploy the new workload:
        //                var response = await DeployVersionArtifactAsync(newLoad);

        //                // Undeploy the old workload:
        //                var response2 = await UndeployVersionArtifactAsync(oldLoad);
        //            }
        //        }
        //    }

        //    var endTime = DateTime.Now.TimeOfDay;

        //    return (endTime - startTime).ToString();
        //}

        //private async Task<string> CheckCredentialChanges(List<Credentials> changedCreds)
        //{
        //    // Get Admin PlatformCredentials from SOC Rest:
        //    var registrySettings = new List<PlatformRegistryEntry>();
        //    registrySettings.Add(await Agent.GetPlatformRegistryEntry("SystemAdministratorUsername"));
        //    registrySettings.Add(await Agent.GetPlatformRegistryEntry("SaaSGridDefaultServiceAccountUsername"));
        //    registrySettings.Add(await Agent.GetPlatformRegistryEntry("SaaSGridSystemUserName"));

        //    foreach (var regValue in registrySettings)
        //    {
        //        if (changedCreds.Any(x => x.UserName == regValue.Value))
        //        {

        //            // changed creds inidcates a change with this reg value, update:
        //            //var credChange = await Agent.SetPlatformRegistryEntry()
        //        }
        //    }


        //    // Validate changed credentials:

        //}

        private void UpdateAdminCredentials(Credentials adminCreds)
        {
            // 
        }

    }
}
